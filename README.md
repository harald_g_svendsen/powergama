# README #

PowerGAMA is a Python package for hour-by-hour optimal power flow analysis of interconnected power systems with variable energy sources and storage systems.

Read more on the [PowerGAMA](https://bitbucket.org/harald_g_svendsen/powergama/wiki/Home) wiki page.
