# -*- coding: utf-8 -*-
'''
PowerGAMA version information
'''

__version__ = '1.1.3'
__version_date__ = '2020-08-26'
